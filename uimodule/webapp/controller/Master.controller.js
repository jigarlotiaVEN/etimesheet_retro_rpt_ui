sap.ui.define([
  "ventia/polaris/etimesheetadj/controller/BaseController",
  "sap/ui/model/FilterOperator",
  "sap/ui/model/Filter",
  "ventia/polaris/etimesheetadj/model/formatter",
  "ventia/polaris/etimesheetadj/model/models",
  "ventia/polaris/etimesheetadj/model/default",
  "sap/ui/core/Fragment",
  "sap/ui/model/Sorter",
  "sap/ui/core/util/Export",
  "sap/ui/core/util/ExportTypeCSV",
  "sap/m/MessageBox",
  "sap/ui/export/Spreadsheet",
], function (Controller, FilterOperator, Filter, formatter, Models, Default, Fragment, Sorter, Export, ExportTypeCSV, MessageBox, Spreadsheet, ) {
  "use strict";
  return Controller.extend("ventia.polaris.etimesheetadj.controller.Master", {
    formatter: formatter,
    onInit: function () {
      this.fnReadTimeData();
      this._mViewSettingsDialogs = {};
    },

    onPersoButtonPressed: function () {
      var columnData = Models.getColumns();
      this.fnGetDataModel().setProperty("/ColumnsItems", columnData.ColumnsItems);
      this.fnGetDataModel().setProperty("/Items", columnData.Items);
      this.fnGetDataModel().setProperty("/ShowResetEnabled", columnData.ShowResetEnabled);
      var sDialog = "ventia.polaris.etimesheetadj.view.Fragments.PersonDialog";
      this.loadDialog(sDialog);
    },


    onFetchReport: function (oEvent) {
      this.fnReadTimeData();
    },

    handleFilterDateChange: function (oEvent) {
      //this.filterReport();
      var aDates = this.fnGetDataModel().getProperty("/Dates");
      this.fnGetDataModel().setProperty("/filterDates", aDates);
      this.fnReadTimeData();
    },

    onSearchPernr: function (event) {
      this.fnReadTimeData();
    },
    onSearchPayrollArea: function (event) {
      this.fnReadTimeData();
    },


    onStatusChange: function (oEvent) {
      var aSelectedData = oEvent.getParameters().selectedItems;
      this.fnGetDataModel().setProperty("/filterStatus", aSelectedData);
      this.filterReport();
    },

    onSearch: function (oEvent) {
      var aFilters = [];
      var aFilter1 = [];
      var oFilter = {};
      var sQuery = oEvent.getSource().getValue();
      if (sQuery && sQuery.length > 0) {
        var searchData = Default.fnsearchFields();
        searchData.forEach(function (o) {
          if (o.type !== "D") {
            oFilter = new Filter(o.name, FilterOperator.Contains, sQuery);
          }
          else {
            oFilter = new Filter(o.name, FilterOperator.EQ, formatter.dateToYYYYMMDDSearch(sQuery));
          }
          aFilters.push(oFilter);
        });
        aFilter1 = new Filter(aFilters, sQuery)
      }

      // update list binding
      var oList = this.byId("idTableData");
      var oBinding = oList.getBinding("items");
      oBinding.filter(aFilter1);

    },

    onPayCodeChange: function (oEvent) {
      var oValue = oEvent.getParameters().value;
      this.fnGetDataModel().setProperty("/filterPayCode", oValue);
      this.filterReport();
    },

    onAllowanceClose: function (oEvent) {
      this.pdialog.close();
    },

    onSeeAllowances: function (oEvent, type) {
      //var oTable = this.getView().byId("idTableData");
      var oSelectedContext = oEvent.getSource().getBindingContext("local");
      if (type == "a") {
        var aData = this.fnGetDataModel().getProperty(oSelectedContext.sPath + "/adjustedAllowances");
      } else {
        var aData = this.fnGetDataModel().getProperty(oSelectedContext.sPath + "/originalAllowances");
      }
      this.fnGetDataModel().setProperty("/allowances", aData);
      var sDialog = "ventia.polaris.etimesheetadj.view.Fragments.AllowanceDialog";
      this.loadDialog(sDialog);

    },

    onSeeComments: function (oEvent, type) {

      var oTable = this.getView().byId("idTableData");
      var oSelectedContext = oEvent.getSource().getBindingContext("local");
      if (type == "a") {
        var aData = this.fnGetDataModel().getProperty(oSelectedContext.sPath + "/adjustedComments");
      } else {
        var aData = this.fnGetDataModel().getProperty(oSelectedContext.sPath + "/originalComments");
      }

      this.fnGetDataModel().setProperty("/comments", aData);


      var sDialog = "ventia.polaris.etimesheetadj.view.Fragments.CommentsDialog";
      if (!this.commentsdialog) {
        Fragment.load({
          id: "CommentsDialog",
          name: sDialog,
          controller: this
        }).then(function (oValueHelpDialog) {
          this.commentsdialog = oValueHelpDialog;
          this.getView().addDependent(this.commentsdialog);
          this.commentsdialog.open();
        }.bind(this));
      }
      else {
        this.commentsdialog.open();
      }
    },

    onCommentDialogClose: function (oEvent) {
      this.commentsdialog.close();
    },
    onCommentsAfterClose: function () {
      this.commentsdialog.destroy();
      this.commentsdialog = null;
    },

    fnFilterTable: function (sQuery) {


    },

    handleSortButtonPressed: function () {
      var sDialog = "ventia.polaris.etimesheetadj.view.Fragments.SortDialog";
      this.getViewSettingsDialog(sDialog)
        .then(function (oViewSettingsDialog) {
          oViewSettingsDialog.open();
        });
    },

    handleSortDialogConfirm: function (oEvent) {
      var oTable = this.byId("idTableData"),
        mParams = oEvent.getParameters(),
        oBinding = oTable.getBinding("items"),
        sPath,
        bDescending,
        aSorters = [];

      sPath = mParams.sortItem.getKey();
      bDescending = mParams.sortDescending;
      aSorters.push(new Sorter(sPath, bDescending));

      // apply the selected sort and group settings
      oBinding.sort(aSorters);
    },

    handleFilterButtonPressed: function () {
      var sDialog = "ventia.polaris.etimesheetadj.view.Fragments.FilterDialog";
      this.getViewSettingsDialog(sDialog)
        .then(function (oViewSettingsDialog) {
          oViewSettingsDialog.open();
        });
    },

    handleFilterDialogConfirm: function (oEvent) {
      var oTable = this.getView().byId("idTableData"),
        mParams = oEvent.getParameters(),
        oBinding = oTable.getBinding("items"),
        aFilters = [];

      mParams.filterItems.forEach(function (oItem) {
        var sPath = oItem.getParent().getKey(),

          sOperator = sap.ui.model.FilterOperator.EQ,
          sValue1 = oItem.getKey(),
          oFilter = new Filter(sPath, sOperator, sValue1, sValue2);
        aFilters.push(oFilter);
      });

    },

    onDataExport: function (oEvent) {

      var aData = this.fnGetDataModel().getProperty("/AdjData");
      var oSettings;
      var oSheet;

      var aColumns = Default.fndownloadData();

      oSettings = {
        workbook: {
          columns: aColumns
        },
        dataSource: aData
      };

      oSheet = new sap.ui.export.Spreadsheet(oSettings);
      oSheet.build()
        .then(function () {
          MessageBox.show("Spreadsheet export has finished");
        })
        .finally(function () {
          oSheet.destroy();
        });
    },

  });

});