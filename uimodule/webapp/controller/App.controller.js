sap.ui.define(["ventia/polaris/etimesheetadj/controller/BaseController"], function (Controller) {
    "use strict";

    return Controller.extend("ventia.polaris.etimesheetadj.controller.App", {
     
      onInit: function () {
      this.oRouter = this.getOwnerComponent().getRouter();
      this.oRouter.attachRouteMatched(this.onRouteMatched, this);
      //this.oRouter.attachBeforeRouteMatched(this.onBeforeRouteMatched, this);
      this.oRouter.navTo("master");
    },

    onRouteMatched: function(oEvent) {

    },

    });
});
