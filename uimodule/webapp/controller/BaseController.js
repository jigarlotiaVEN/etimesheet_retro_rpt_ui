sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/routing/History",
    "sap/ui/core/UIComponent",
    "ventia/polaris/etimesheetadj/model/formatter",
    "ventia/polaris/etimesheetadj/util/ServiceUtil",
    "ventia/polaris/etimesheetadj/util/Config",
    "sap/ui/model/FilterOperator",
    "sap/ui/model/Filter",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "sap/ui/core/Fragment",
    "sap/ui/Device",
  ],
  function (Controller, History, UIComponent, formatter, ServiceUtil, Config, FilterOperator, Filter, MessageBox, MessageToast, Fragment, Device) {
    "use strict";

    return Controller.extend("ventia.polaris.etimesheetadj.controller.BaseController", {
      formatter: formatter,

      /**
       * Convenience method for getting the view model by name in every controller of the application.
       * @public
       * @param {string} sName the model name
       * @returns {sap.ui.model.Model} the model instance
       */
      getModel: function (sName) {
        return this.getView().getModel(sName);
      },

      /**
       * Convenience method for setting the view model in every controller of the application.
       * @public
       * @param {sap.ui.model.Model} oModel the model instance
       * @param {string} sName the model name
       * @returns {sap.ui.mvc.View} the view instance
       */
      setModel: function (oModel, sName) {
        return this.getView().setModel(oModel, sName);
      },

      /**
       * Convenience method for getting the resource bundle.
       * @public
       * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
       */
      getResourceBundle: function () {
        return this.getOwnerComponent().getModel("i18n").getResourceBundle();
      },

      /**
       * Method for navigation to specific view
       * @public
       * @param {string} psTarget Parameter containing the string for the target navigation
       * @param {Object.<string, string>} pmParameters? Parameters for navigation
       * @param {boolean} pbReplace? Defines if the hash should be replaced (no browser history entry) or set (browser history entry)
       */
      navTo: function (psTarget, pmParameters, pbReplace) {
        this.getRouter().navTo(psTarget, pmParameters, pbReplace);
      },

      getRouter: function () {
        return UIComponent.getRouterFor(this);
      },

      fnGetTokenModel: function () {
        return this.getOwnerComponent().getModel("Token");
      },
      fnGetDataModel: function () {
        return this.getOwnerComponent().getModel("local");
      },
      fnServiceUtil: function () {
        return ServiceUtil;
      },
      fnConfig: function () {
        return Config;
      },

      fnFilter: function () {
        return Filter;
      },

      //Retrieve Time Data
      fnReadTimeData: function () {
        var oParams = this.fnsetDate();
        //make view visible
        this.getView().setBusy(true);
        var toRetrieveTimesheetData = this.fnServiceUtil().fnAjaxQuery(this.fnConfig().getAdjData(), "GET", oParams);
        toRetrieveTimesheetData.then(function (data) {
          data.forEach(function (o) {
            o.Hasallowancea = false;
            o.Hasallowanceo = false;
            o.Hascommentsa = false;
            o.Hascommentso = false;
            if (o.adjustedAllowances.length > 0){
              o.Hasallowancea = true;
            }
            if (o.originalAllowances.length > 0){
              o.Hasallowanceo = true;
            }    
            if (o.adjustedComments.length > 0){
              o.Hascommentsa = true;
            }   
            if (o.originalComments.length > 0){
              o.Hascommentso = true;
            }                             

          });
          this.filterReport();
          this.fnGetDataModel().setProperty("/AdjData", data);
          this.getView().setBusy(false);
        }.bind(this)).catch(function (oError) {
          // // console.log(e.message);
          this.getView().setBusy(false);
          try {
            MessageBox.error(oError.status + " ERROR: " + oError.responseJSON.message);
          } catch (error) {
            MessageBox.error(oError.statusText);
          }
        }.bind(this));
      },

      fnsetDate: function (oEvent) {
        var oDates = this.fnGetDataModel().getProperty("/Dates");
        if (oDates) {
          var firstDay = oDates.begda;
          var lastDay = oDates.endda;
        } else {
          var date = new Date();
          var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
          var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        }
        var oParams = {};
        oParams.begda = formatter.dateToYYYYMMDD(firstDay);
        oParams.endda = formatter.dateToYYYYMMDD(lastDay);

        var Dates = {};
        Dates.begda = firstDay;
        Dates.endda = lastDay;
        //set date model for date range
        this.fnGetDataModel().setProperty('/Dates', Dates);

        return oParams;
      },
      filterReport: function () {
        var aFilter = [];
        var aPernr = this.fnGetDataModel().getProperty("/PERNR");
        if (aPernr) {
          aFilter.push(new Filter("employeeID", FilterOperator.Contains, aPernr));
        }

        var oList = this.byId("idTableData");
        var oBinding = oList.getBinding("items");
        oBinding.filter(aFilter);

      },

      getViewSettingsDialog: function (sDialogFragmentName) {
        var pDialog = this._mViewSettingsDialogs[sDialogFragmentName];

        if (!pDialog) {
          pDialog = Fragment.load({
            id: this.getView().getId(),
            name: sDialogFragmentName,
            controller: this
          }).then(function (oDialog) {
            if (Device.system.desktop) {
              oDialog.addStyleClass("sapUiSizeCompact");
            }
            return oDialog;
          });
          this._mViewSettingsDialogs[sDialogFragmentName] = pDialog;
        }
        return pDialog;
      },

      loadDialog: function (sFragment) {

        if (!this.pdialog) {
          Fragment.load({
            id: this.getView().getId(),
            name: sFragment,
            controller: this
          }).then(function (oValueHelpDialog) {
            this.pdialog = oValueHelpDialog;
            this.getView().addDependent(this.pdialog);
            this.pdialog.open();
          }.bind(this));
        }
        else {
          this.pdialog.open();
        }

      },

      onNavBack: function () {
        var sPreviousHash = History.getInstance().getPreviousHash();

        if (sPreviousHash !== undefined) {
          window.history.back();
        } else {
          this.getRouter().navTo("appHome", {}, true /*no history*/);
        }
      }
    });
  }
);
