sap.ui.define(["sap/ui/model/json/JSONModel", "sap/ui/Device"], function (JSONModel, Device) {
  "use strict";

  return {

    fndownloadData: function () {
      var Object = {};
      var downloadData = [];
      Object.label = "Employee Name";
      Object.property = "employeeName";
      downloadData.push(Object);
      Object = {};
      Object.label = "Employee ID";
      Object.property = "employeeID";
      downloadData.push(Object);
      Object = {};
      Object.label = "Status";
      Object.property = "adjustedStatus";
      downloadData.push(Object);
      Object = {};
      Object.label = "Created By";
      Object.property = "adjustedCreatedBy";
      downloadData.push(Object);
      Object = {};
      Object.label = "Created On";
      Object.property = "adjustedCreatedOn";
      downloadData.push(Object);
      Object = {};
      Object.label = "Changed By";
      Object.property = "adjustedChangedBy";
      downloadData.push(Object);
      Object = {};
      Object.label = "Changed On";
      Object.property = "adjustedChangedOn";
      downloadData.push(Object);
      Object = {};
      Object.label = "Date";
      Object.property = "timesheetDate";
      downloadData.push(Object);
      Object = {};
      Object.label = "Time In (Orig)";
      Object.property = "originalTimeIn";
      downloadData.push(Object);
      Object = {};
      Object.label = "Time In (New)";
      Object.property = "adjustedTimeIn";
      downloadData.push(Object);
      Object = {};
      Object.label = "Time Out (Orig)";
      Object.property = "originalTimeOut";
      downloadData.push(Object);
      Object = {};
      Object.label = "Time Out (New)";
      Object.property = "adjustedTimeOut";
      downloadData.push(Object);

      Object = {};
      Object.label = "Actual Hours (Orig)";
      Object.property = "originalActualHours";
      downloadData.push(Object);
      Object = {};
      Object.label = "Actual Hours (New)";
      Object.property = "adjustedActualHours";
      downloadData.push(Object);
      Object = {};
      Object.label = "Pay Type (Orig)";
      Object.property = "originalPayType";
      downloadData.push(Object);
      Object = {};
      Object.label = "Pay Type (Orig)";
      Object.property = "adjustedPayType";
      downloadData.push(Object);
      Object = {};
      Object.label = "Pay Code (Orig)";
      Object.property = "originalPayCode";
      downloadData.push(Object);
      Object = {};
      Object.label = "Pay Code Desc (Orig)";
      Object.property = "originalPayCodeDesc";
      downloadData.push(Object);
      Object = {};
      Object.label = "Pay Code (New)";
      Object.property = "adjustedPayCode";
      downloadData.push(Object);
      Object = {};
      Object.label = "Pay Code Desc(New)";
      Object.property = "adjustedPayCodeDesc";
      downloadData.push(Object);
      Object = {};
      Object.label = "Cost Object Type (Orig)";
      Object.property = "originalCostObjectType";
      downloadData.push(Object);
      Object = {};
      Object.label = "Cost Object Type (New)";
      Object.property = "adjustedCostObjectType";
      downloadData.push(Object);
      Object = {};
      Object.label = "Cost Object (Orig)";
      Object.property = "{local>originalParentCostObject}";
      downloadData.push(Object);
      Object = {};
      Object.label = "Cost Object (New)";
      Object.property = "{local>adjustedParentCostObject}";
      downloadData.push(Object);
      Object = {};
      Object.label = "Operation/Activity (Orig)";
      Object.property = "{local>originalChildCostObject}";
      downloadData.push(Object);
      Object = {};
      Object.label = "Operation/Activity (Orig)";
      Object.property = "{local>adjustedChildCostObject}";
      downloadData.push(Object);
      Object = {};
      Object.label = "Higher Duty (Orig)";
      Object.property = "originalHigherDuty";
      downloadData.push(Object);
      Object = {};
      Object.label = "Higher Duty (New)";
      Object.property = "adjustedHigherDuty";
      downloadData.push(Object);
      Object = {};
      Object.label = "Allowances (Orig)";
      Object.property = "";
      downloadData.push(Object);
      Object = {};
      Object.label = "Allowances (New)";
      Object.property = "";
      downloadData.push(Object);
      Object = {};
      Object.label = "Comments (Orig)";
      Object.property = "";
      downloadData.push(Object);
      Object = {};
      Object.label = "Comments (New)";
      Object.property = "";
      downloadData.push(Object);
      return downloadData;
    },

    fnsearchFields: function () {
      var Object = {};
      var searchFields = [];
      Object.name = "employeeName";
      Object.type = "";
      searchFields.push(Object);
      Object = {};
      Object.name = "employeeID";
      Object.type = "";
      searchFields.push(Object);
      Object = {};
      Object.name = "adjustedStatus";
      Object.type = "";
      searchFields.push(Object);
      Object = {};
      Object.name = "adjustedCreatedBy";
      Object.type = "";
      searchFields.push(Object);
      Object = {};
      Object.name = "adjustedCreatedOn";
      Object.type = "D";
      searchFields.push(Object);
      Object = {};
      Object.name = "adjustedChangedBy";
      Object.type = "";
      searchFields.push(Object);
      Object = {};
      Object.name = "adjustedChangedOn";
      Object.type = "D";
      searchFields.push(Object);
      Object = {};
      Object.name = "originalTimeIn";
      Object.type = "";
      searchFields.push(Object);
      Object = {};
      Object.name = "adjustedTimeIn";
      Object.type = "";
      searchFields.push(Object);
      Object = {};
      Object.name = "originalTimeOut";
      Object.type = "";
      searchFields.push(Object);
      Object = {};
      Object.name = "adjustedTimeOut";
      Object.type = "";
      searchFields.push(Object);
      Object = {};
      Object.name = "originalActualHours";
      Object.type = "";
      searchFields.push(Object);

      Object = {};
      Object.name = "adjustedActualHours";
      Object.type = "";
      searchFields.push(Object);
      Object = {};
      Object.name = "originalPayType";
      Object.type = "";
      searchFields.push(Object);
      Object = {};
      Object.name = "adjustedPayType";
      Object.type = "";
      searchFields.push(Object);
      Object = {};
      Object.name = "originalPayCode";
      Object.type = "";
      searchFields.push(Object);
      Object = {};
      Object.name = "adjustedPayCode";
      Object.type = "";
      searchFields.push(Object);
      Object = {};
      Object.name = "originalCostObjectType";
      Object.type = "";
      searchFields.push(Object);
      Object = {};
      Object.name = "adjustedCostObjectType";
      Object.type = "";
      searchFields.push(Object);
      Object = {};
      Object.name = "originalParentCostObject";
      Object.type = "";
      searchFields.push(Object);
      Object = {};
      Object.name = "adjustedParentCostObject";
      Object.type = "";
      searchFields.push(Object);
      Object = {};
      Object.name = "originalChildCostObject";
      Object.type = "";
      searchFields.push(Object);
      Object = {};
      Object.name = "adjustedChildCostObject";
      Object.type = "";
      searchFields.push(Object);
      //Object = {};
      //Object.name = "originalHigherDuty";
      //Object.type = "";
      //searchFields.push(Object);
      //Object = {};
      //Object.name = "adjustedHigherDuty";
      //Object.type = "";
      //searchFields.push(Object);

      return searchFields;
    },
  };
});